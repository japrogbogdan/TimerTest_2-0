package com.example.bogdan.timertest;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;


/**
 * Created by Beohrn on 21.09.2015.
 */
public class SharedPreference {

    public static final String PREFERENCE_NAME = "PREFERENCE_SETTINGS";
    public static final String ITEM = "ITEM";

    public SharedPreference() {
        super();
    }

    public void saveTask(Context context, List<Product> productList) {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;

        preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        Gson gson = new Gson();
        String jsonTasks = gson.toJson(productList);
        editor.putString(ITEM, jsonTasks);
        editor.commit();
        Log.i("Saved Tasks", "Saved list >>>>> " + productList);
    }

    public ArrayList<Product> getTasks(Context context) {
        SharedPreferences preferences;
        List<Product> taskList;

        preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (preferences.contains(ITEM)) {
            String jsonItem = preferences.getString(ITEM, null);
            Gson gson = new Gson();
            Product[] taskItems = gson.fromJson(jsonItem, Product[].class);
            taskList = Arrays.asList(taskItems);
            taskList = new ArrayList<Product>(taskList);
        } else
            return null;

        Log.d("List", ">>>> " + taskList);


        return (ArrayList<Product>) taskList;
    }

    public void addTask(Context context, Product product) {
        List<Product> tasks = getTasks(context);
        if (tasks == null)
            tasks = new ArrayList<Product>();
        tasks.add(product);
        saveTask(context, tasks);
    }

    public void removeTask(Context context, Product product) {
        ArrayList<Product> products = getTasks(context);
        if (product != null) {
            products.remove(product);
            saveTask(context, products);
            Log.i("Tag", ">>>>>>>>>>>>" + products.size());
        }
    }

}
