package com.example.bogdan.timertest;

/**
 * Created by Beohrn on 04.10.2015.
 */
public class Product {

    private long time;
    private String description;
    private String taskName;
    private boolean taskCheck;
    private long selectTime;
    private Integer progress;

    public Product(long time, String taskName, long selectTime) {
        super();
        this.time = time;
        this.taskName = taskName;
        this.selectTime = selectTime;
//        this.progress = progress;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public boolean isTaskCheck() {
        return taskCheck;
    }

    public void setTaskCheck(boolean taskCheck) {
        this.taskCheck = taskCheck;
    }

    public long getSelectTime() {
        return selectTime;
    }

    public void setSelectTime(long selectTime) {
        this.selectTime = selectTime;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Product))
            return false;
        Product product = (Product) o;
        return taskName.equals(product.taskName);
    }

    @Override
    public int hashCode() {
        return taskName.hashCode();
    }

    @Override
    public String toString() {
        return "Product [time=" + time + "; Tast name=" + taskName + "]";

    }


}
