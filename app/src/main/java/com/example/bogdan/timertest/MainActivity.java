package com.example.bogdan.timertest;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Product> list;


    SharedPreference preference;
    Product product;
    Intent myIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //product = new Product();
        list = new ArrayList<Product>();
        preference = new SharedPreference();
        create(preference.getTasks(this));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.buttonAddTimeTask);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getApplicationContext(), ChoseTime.class);
                startActivity(intent);
            }
        });
        myIntent = getIntent();
        Long time = myIntent.getLongExtra("time", 0);
        String nameTitle = myIntent.getStringExtra("nameTitle");

        if (time != 0) {
            createFragment(time, nameTitle);
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMain);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Drawer rezult = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName("home")
                                .withIdentifier(1),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem()
                                .withName("home 2")
                                .withIdentifier(1),
                        new SecondaryDrawerItem()
                                .withName("item 1"),
                        new SecondaryDrawerItem()
                                .withName("item 1"),
                        new SecondaryDrawerItem()
                                .withName("item 1")
                )
                .build();


    }

    public void createFragment(long time, String nameTitle) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        product = new Product(time, nameTitle, myIntent.getLongExtra("selectTime", 0));
        list.add(product);
        BlankFragment fm2 = new BlankFragment(nameTitle, time, product);
        fragmentTransaction.add(R.id.fragmentContainer, fm2, "HELLO");
        fragmentTransaction.commit();
        //String timeStr = String.valueOf(time);
        //myIntent = getIntent();

        preference.addTask(MainActivity.this, product);


    }

    public void create(ArrayList<Product> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {

                product = list.get(i);
                long buf = product.getSelectTime() - System.currentTimeMillis();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                BlankFragment fm2 = new BlankFragment(product.getTaskName(), buf, product);
                fragmentTransaction.add(R.id.fragmentContainer, fm2, "HELLO");
                fragmentTransaction.commit();
            }
        }

    }




    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}