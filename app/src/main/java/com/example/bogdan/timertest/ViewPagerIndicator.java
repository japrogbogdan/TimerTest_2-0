package com.example.bogdan.timertest;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ViewPagerIndicator extends FragmentActivity {

    static final String TAG = "myLogs";
    static final int PAGE_COUNT = 3;
    int paddingIcon = 7;

    ViewPager pager;
    PagerAdapter pagerAdapter;
    Button buttonNext;


    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_indicator);

        final ImageView button1 = (ImageView) findViewById(R.id.text1);
        final ImageView button2 = (ImageView) findViewById(R.id.text2);
        final ImageView button3 = (ImageView) findViewById(R.id.text3);
        buttonNext = (Button) findViewById(R.id.buttonNext);



        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);


            }
        });
        button2.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
        button3.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected, position = " + position);

                switch (position) {
                    case 0:

                        button1.setPadding(0, 0, 0, 0);
                        button2.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
                        button3.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
                        break;

                    case 1:
                        button1.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
                        button2.setPadding(0, 0, 0, 0);
                        button3.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
                        break;

                    case 2:
                        button1.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
                        button3.setPadding(0, 0, 0, 0);
                        button2.setPadding(paddingIcon, paddingIcon, paddingIcon, paddingIcon);
                        break;

                    case 3:
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

    }

}
