package com.example.bogdan.timertest;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChoseTime extends Activity {

    EditText editTextData;
    EditText editTextTime;

    ScrollView scrollView;


    int DIALOG_TIME_DATA = 1;
    int DIALOG_TIME_CLOCK = 2;
    int myHour = 14;
    int myMinute = 35;
    int myYear = 2015;
    int myMonth = 8;
    int myDay = 29;
    Calendar c;
    long timeFinal = 0;
    long bufTime = 0;

    List<String> list = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_time);

        Button button = (Button) findViewById(R.id.buttonStart);
        Button buttonAddTask = (Button) findViewById(R.id.addTask);

        editTextData = (EditText) findViewById(R.id.edit_Data);
        editTextTime = (EditText) findViewById(R.id.edit_Time);

        final EditText editText = (EditText) findViewById(R.id.editTextTitle);
        final EditText taskText = (EditText) findViewById(R.id.taskText);

        ScrollView view = (ScrollView)findViewById(R.id.scrollViewTask);
        view.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.requestFocusFromTouch();

                scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scrollView.post(new Runnable() {
                            public void run() {
                                scrollView.fullScroll(View.FOCUS_DOWN);
                            }
                        });
                    }
                });

                return false;
            }
        });

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        buttonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final LinearLayout layout = (LinearLayout) findViewById(R.id.llfortask);

                LayoutInflater inflater =
                        (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View view1 = inflater.inflate(R.layout.taskfragmentinlayout, null);

                TextView textView = (TextView)view1.findViewById(R.id.textFragment);
                textView.setText(taskText.getText().toString());
                taskText.setText("");
                taskText.setHint("довавь еще ))");

                view1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout.removeView(v);
                    }
                });

                layout.addView(view1);
            }
        });



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("nameTitle", editText.getText().toString());
                intent.putExtra("time", timeFinal);
                intent.putExtra("selectTime", bufTime);
                //intent.putExtra("buf", bufTime);
                startActivity(intent);
                finish();
            }
        });
    }



    public void onclickTime(View view) {
        showDialog(DIALOG_TIME_CLOCK);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_TIME_CLOCK) {
            TimePickerDialog tpd = new TimePickerDialog(this, myCallBack, myHour, myMinute, true);
            return tpd;
        }

        if (id == DIALOG_TIME_DATA) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBackData, myYear, myMonth, myDay);
            return tpd;
        }

        return super.onCreateDialog(id);
    }

    TimePickerDialog.OnTimeSetListener myCallBack = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {



            myHour = hourOfDay;
            myMinute = minute;
            Log.d("Teg", "Time is " + myHour + " hours " + myMinute + " minutes "
                    + (((myHour * 60 * 60 * 1000) + (myMinute * 60 * 1000)) + System.currentTimeMillis()));

            editTextTime.setText(myHour + "." + myMinute);

            c = Calendar.getInstance();
            c.set(myYear, myMonth, myDay, myHour, myMinute);
            bufTime = c.getTimeInMillis();
            timeFinal = bufTime - System.currentTimeMillis();
        }
    };


    public void onclickData(View view) {
        showDialog(DIALOG_TIME_DATA);
    }

    DatePickerDialog.OnDateSetListener myCallBackData = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String tempMoth = null;

            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;

            if(myMonth < 9){
                int tempMothInteger = myMonth + 1;
                tempMoth = "0" + tempMothInteger;
                editTextData.setText(myDay + "." + tempMoth + "." + myYear);
            } else {
                int tempMothInteger = myMonth + 1;
                editTextData.setText(myDay + "." + tempMothInteger + "." + myYear);
            }




            c = Calendar.getInstance();
            c.set(myYear, myMonth, myDay, myHour, myMinute);
            long d = c.getTimeInMillis();
            timeFinal = d - System.currentTimeMillis();

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chose_time, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
