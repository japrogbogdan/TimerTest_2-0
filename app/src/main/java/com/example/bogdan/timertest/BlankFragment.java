package com.example.bogdan.timertest;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


@SuppressLint("ValidFragment")
public class BlankFragment extends Fragment {

    private String m;
    private Long s;
    private SharedPreference sharedPreference;
    private ProgressBar progressBar;
    private Product product;



    public BlankFragment(String m, Long s, Product product ) {
        this.m = m;
        this.s = s;
        sharedPreference = new SharedPreference();
        this.product = product;
    }



    class ProgressBarTime extends AsyncTask<Void, Integer, Void> {

        int progress = 0;


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            progressBar.setProgress(values[0]);
        }

        @Override
        protected Void doInBackground(final Void... params) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    while (progress < s/1000) {
                        progress++;
                        publishProgress(progress);
                        SystemClock.sleep(1000);
                    }
                }
            }).start();

            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_blank, null);


        final TextView mTimer = (TextView) v.findViewById(R.id.textViewClock);
        final TextView nameTitle = (TextView) v.findViewById(R.id.nameTitle);
        final Button button = (Button) v.findViewById(R.id.clearFragment);

        nameTitle.setText(m);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBarTime);
        long temp = s/1000;
        progressBar.setMax((int) temp);

//        handler =  new Handler() {
//            public void handleMessage(android.os.Message msg) {
//                progress = (msg.what);
//            }
//        };

        new ProgressBarTime().execute();


        Log.d("Tick1", Math.abs(s) + " йобаные секунды ");


        final CountDownTimer timer = new CountDownTimer(s, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds;
                int minutes;
                int hours;
                int days;
                seconds = (int) (millisUntilFinished / 1000) % 60;
                minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
                days = (int) ((millisUntilFinished / (1000 * 60 * 60 * 24)) % 365);
                mTimer.setText(days + " days " + hours + " hrs " + minutes + " mins " + seconds + " sec");
                Log.d("Tick", days + " days " + hours + " hrs " + minutes + " mins " + seconds + " sec ");
            }

            public void onFinish() {
                mTimer.setText("Бабах!");
            }
        };

        timer.start();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(v.getContext())
                        .setTitle(m)
                        .setMessage("Are you sure you want to delete this entry?&&&&&")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                getActivity().getFragmentManager().beginTransaction().remove(BlankFragment.this).commit();
                                timer.cancel();
                                sharedPreference.removeTask(getActivity(), product);

                                Log.d("List", ">>>> " + product);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                new AlertDialog.Builder(v.getContext())
                        .setTitle("Delete entry")
                        .setMessage("Are you sure you want to delete this entry?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                getActivity().getFragmentManager().beginTransaction().remove(BlankFragment.this).commit();
                                timer.cancel();
//                                Product product = new Product();
                                //sharedPreference.removeTask(getActivity(), str);
                                sharedPreference.removeTask(getActivity(), product);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                return false;
            }
        });
        return v;
    }




}
